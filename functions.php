<?php

require_once __DIR__ . '/functions/post-types.php';

function custom_css_include()
{
	wp_register_style( 
		'custom-css', 
		get_stylesheet_directory_uri() . '/style.css', 
		array(), 
		'20121207', 
		'all'
	);
	wp_enqueue_style( 'custom-css' );
}
add_action( 'wp_enqueue_scripts', 'custom_css_include', 10 );

function custom_js_include()
{
	wp_register_script( 
		'custom-js', 
		get_stylesheet_directory_uri() . '/custom.js', 
		array('jquery'), 
		'20151207', 
		true 
	);
	wp_enqueue_script( 'custom-js' );
}
add_action( 'wp_enqueue_scripts', 'custom_js_include' );

function theme_init()
{
	register_nav_menus(array(
		'nav-menu' => 'Nav Menu',
		'nav-menu-two' => 'Nav Menu Two',
	));
}
add_action( 'init', 'theme_init' );

// found in functions/post-types.php
function register_custom_post_types()
{
	our_custom_post_type();
	our_second_custom_post_type();
}
add_action( 'init', 'register_custom_post_types', 0 );

/**
 * Adds a box to the main column on the Post and Page edit screens.
 */
function myplugin_add_meta_box() 
{

	add_meta_box( 
		'events-metabox', 
		'Event Options', 
		'events_metabox_callback', 
		'custom-post-type'
	);
}
add_action( 'add_meta_boxes', 'myplugin_add_meta_box' );

/**
 * Prints the box content.
 * 
 * @param WP_Post $post The object for the current post/page.
 */
function events_metabox_callback($post)
{
	wp_nonce_field( 'myplugin_save_meta_box_data', 'myplugin_meta_box_nonce' );

	?>
		<!-- Start date field -->
		<p><label for="event_start_date">Start Date</label></p>
		<p><input type="text" name="event_start_date" value="<?php echo get_post_meta($post->ID, 'event_start_date', true); ?>"></p>

		<!-- End date field -->
		<p><label for="event_start_date">End Date</label></p>
		<p><input type="text" name="event_end_date" value="<?php echo get_post_meta($post->ID, 'event_end_date', true); ?>"></p>
	<?php
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function events_save_post_date($post_id)
{
	if( ! isset( $_POST['myplugin_meta_box_nonce'] ) ) {
		return;
	}

	if( isset( $_POST['event_start_date'] ) ) {
		update_post_meta( $post_id, 'event_start_date', $_POST['event_start_date'] );
	}

	if( isset( $_POST['event_end_date'] ) ) {
		update_post_meta( $post_id, 'event_end_date', $_POST['event_end_date'] );
	}
}
add_action( 'save_post', 'events_save_post_date' );
